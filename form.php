<?php

require_once dirname(__FILE__).'/api/config.php';  //подключение класов для работ c API

// api data
$USER_LOGIN = 'russellorv@gmail.com';
$USER_HASH = 'fa21abc306ac55d0a90f549568c646692b9d8aed';
$subdomain = 'russellorv';
$phone_field_id = 403553;
$email_field_id = 403555;


$contact_name = isset($_POST['name']) ? htmlentities($_POST['name']) : '' ;
$contact_email = isset($_POST['email']) ? htmlentities($_POST['email']) : '' ;
$contact_phone = isset($_POST['phone']) ? htmlentities($_POST['phone']) : '' ;

if (empty($contact_email) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $contact_email) ){
    $error = true;
}

if ((mb_strlen($contact_phone) < 3) || (mb_strlen($contact_phone) > 32)) {
    $error = true;
}

if(isset($error)){
    header("Refresh:0; url=index.php?error=1"); die();
}

$api = new \AmoCrm\Client($USER_LOGIN, $USER_HASH, $subdomain);

// ищем контакт
$find_contact = [];

$return = $api->getContacts(array('query' => $contact_email));
$find_contact = ($return->isSuccessful()) ? $return->getData()['_embedded']['items'][0] : [];

if(!$return->isSuccessful()){
   $return = $api->getContacts(array('query' => $contact_phone ));
   $find_contact = ($return->isSuccessful()) ? $return->getData()['_embedded']['items'][0] : [];
}

// если не нашли создаем контакт
if(!$find_contact){

// пользователь с min count сделок
$responsible_user_id = $api->getUserMinLeads();

$contact = array(
   array(
        'name' => $contact_name,
        'responsible_user_id' => $responsible_user_id,
        'created_by' => $responsible_user_id,
        'custom_fields' => array(
            array(
                'id' => $phone_field_id,
                'values' => array(
                   array(
                      'value' => $contact_phone,
                      'enum' => "WORK"
                   )
                )
            ),
            array(
                'id' => $email_field_id,
                'values' => array(
                   array(
                      'value' => $contact_email,
                      'enum' => "WORK"
                   )
                )
            )
        )
    )
);

// создаем контакт
$return = $api->createContact($contact);

$find_contact = ($return->isSuccessful()) ? $return->getData()['_embedded']['items'][0] : [];
$find_contact['responsible_user_id'] = $responsible_user_id;

}

if(!$find_contact){
    header("Refresh:0; url=index.php?false=1"); die();
}

// добавляем сделку

$lead = array(
    array(
        'name' => 'Заявка с сайта',
        'responsible_user_id' => $find_contact['responsible_user_id'],
        'contacts_id' => $find_contact['id'],
    )
);

$return = $api->createLead($lead);
$find_lead = ($return->isSuccessful()) ? $return->getData()['_embedded']['items'][0] : [];

if(!$find_lead){
    header("Refresh:0; url=index.php?false=1"); die();
}

// добавляем Задачу перезвонить
$task = array(
    array(
        'element_id'          => $find_lead['id'], #ID сделки
        'element_type'        => 2, #Показываем, что это - сделка, а не контакт
        'task_type'           => 1, #Звонок
        'text'                => 'Перезвонить клиенту',
        'responsible_user_id' => $find_contact['responsible_user_id'],
        'complete_till_at'    => strtotime( date('Y-m-d H:i:s') . " +1 days")
    )
);

$return = $api->createTask($task);

if($return->isSuccessful()){
    $_SESSION['success'] = true;
    header("Refresh:0; url=index.php?success=1"); die();
}

header("Refresh:0; url=index.php"); die();



