<?php

namespace AmoCrm;

use AmoCrm\ApiResponse;

/**
 * Trait Contact Часть API-клиента, отвечающая за Contact

 */
trait Contact
{
    //Получение контакта

    public function getContact( $id) 
    {
        return $this->client->executeGetRequest(
            $this->generateUrl(self::API_URL_CONTACTS, $id),
            $id
        );
    }

    //Получение контактов
    
    public function getContacts( $params = []) 
    {
        return $this->client->executeListRequest($this->generateUrl(self::API_URL_CONTACTS), $params);
    }

    //Создание контакта
    
    public function createContact( $data ) 
    {
        return $this->client->executePostRequest(
            $this->generateUrl(self::API_URL_CONTACTS),
            array('add' => $data)
        );
    }
}
