<?php

namespace AmoCrm;

use AmoCrm\ApiResponse;

/**
 * Trait Lead Часть API-клиента, отвечающая за Сделки
 */
trait Lead
{
    //Получение Сделки
    
    public function getLead( $id) 
    {
        return $this->client->executeGetRequest(
            $this->generateUrl(self::API_URL_LEADS, $id),
            $id
        );
    }

    //Получение Сделки

    public function getLeads( $params = []) 
    {
        return $this->client->executeListRequest($this->generateUrl(self::API_URL_LEADS), $params);
    }


    //Создание Сделки

    public function createLead( $data ) 
    {
        return $this->client->executePostRequest(
            $this->generateUrl(self::API_URL_LEADS),
            array('add' => $data)
        );
    }
}
