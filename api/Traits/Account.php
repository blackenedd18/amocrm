<?php

namespace AmoCrm;

use AmoCrm\ApiResponse;

/**
 * Trait Account Часть API-клиента, отвечающая за Account
 */
trait Account
{
    //Получение данних всего Account

    public function getAccountData( $params = []) 
    {
        return $this->client->executeListRequest($this->generateUrl(self::API_URL_ACCOUNT), $params);
    }

}
