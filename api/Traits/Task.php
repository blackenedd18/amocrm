<?php

namespace AmoCrm;

use AmoCrm\ApiResponse;

/**
 * Trait Task Часть API-клиента, отвечающая за задачи
 */
trait Task
{
    //Получение задачи

    public function getTask( $id) 
    {
        return $this->client->executeGetRequest(
            $this->generateUrl(self::API_URL_TASKS, $id),
            $id
        );
    }

    //Получение задач

    public function getTasks( $params = []) 
    {
        return $this->client->executeListRequest($this->generateUrl(self::API_URL_TASKS), $params);
    }


    //Создание задачи

    public function createTask( $data ) 
    {
        return $this->client->executePostRequest(
            $this->generateUrl(self::API_URL_TASKS),
            array('add' => $data)
        );
    }
}
