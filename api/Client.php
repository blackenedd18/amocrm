<?php

namespace AmoCrm;

use AmoCrm\HttpClient;

/**
 * Class Client
 * служит для работы с API
 * blackenedd18@gmail.com
 */
class Client
{
    use Contact, Lead, Task , Account;
    
    const API_URL_CONTACTS = 'api/v2/contacts/';
    const API_URL_LEADS = 'api/v2/leads/';
    const API_URL_TASKS = 'api/v2/tasks/';
    const API_URL_ACCOUNT = 'api/v2/account/';
    
    private $client;

    //Инициализация Клиента
    public function __construct( $USER_LOGIN,  $USER_HASH,  $subdomain)
    {
        $this->client = new HttpClient($USER_LOGIN, $USER_HASH, $subdomain);

    }

    // проверка на авторизацию пользователя
    public function isLogin()
    {
        return $this->client->getIsLogin();

    }

    // получение всех менеджеров
    public function getUsersAccountIds()
    {
        $all_users = array();
        $return = $this->getAccountData( array('with' => 'users') );
        
        if($return->isSuccessful()){
            $data = $return->getData();
            foreach($data['_embedded']['users'] as $user){
                if(!$user['is_admin']) $all_users[$user['id']] = 0;
            }
        }

        return $all_users;
    }

    // получение менеджера с min count заявок
    public function getUserMinLeads()
    {
        $leads_to_users = $this->getUsersAccountIds();
            
        $all_leads = $this->getLeads(array('filter' => array('date_create' => array('from' => strtotime( date('Y-m-d H:i:s') . " -1 days")))));

        if($all_leads->isSuccessful()){
        
            $data_leads = $all_leads->getData();
            $find_user = array();
            foreach($data_leads['_embedded']['items'] as $lead){
                if(isset($lead['main_contact']['id']) && !in_array($lead['main_contact']['id'],$find_user)){

                    if(isset($leads_to_users[$lead['responsible_user_id']])){
                        $leads_to_users[$lead['responsible_user_id']]++;
                    }

                    $find_user[] = $lead['main_contact']['id'];
                }
            }
        }
        asort($leads_to_users);
        
        $keys = array_keys($leads_to_users);
        $firstKey = $keys[0];

        return $firstKey;
      
    }

    //Генерация URL для API
    private function generateUrl( $url, $id = null)
    {
        if ($id === null) {
            $result = $this->client->getBaseUrl() . $url ;
        } else {
            $result = $this->client->getBaseUrl() . $url . '/' . $id ;
        }
        return $result;
    }


}
