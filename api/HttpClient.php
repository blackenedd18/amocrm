<?php

namespace AmoCrm;

use AmoCrm\ApiResponse;

/**
 * Class Client
 */
class HttpClient
{
    /** Описание возможный методов запросов*/
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    const API_URL_LOGIN = 'private/api/auth.php?type=json';

    private $baseUrl;
    private $isLogin;

    // Инициализация Клиента
    
    public function __construct( $USER_LOGIN, $USER_HASH, $subdomain ) {
       
        $this->baseUrl = 'https://'.$subdomain.'.amocrm.ru/';

        $this->isLogin = $this->login($USER_LOGIN, $USER_HASH);
        
    }


    public function getBaseUrl()
    {
        return (string)$this->baseUrl;
    }
    
    public function getIsLogin()
    {
        return $this->isLogin;
    }

    //авторизация пользователя
    
    private function login( $USER_LOGIN, $USER_HASH ){
        
        $data = array(
          'USER_LOGIN'=> $USER_LOGIN,
          'USER_HASH'=> $USER_HASH
        );

        $result = $this->executePostRequest( $this->baseUrl . self::API_URL_LOGIN , $data );

        $data = $result->getData();

        return isset($data['response']['user']['id']) ? $data['response']['user']['id'] : 0;
    }


    // Выполнение базового запроса

    public function request( $method,  $url,  $params = '',  $headers = [])
    {

        $responseHeaders =  array('Content-Type: application/json');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        curl_setopt($ch,CURLOPT_HEADER,false);
        curl_setopt($ch,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); 
        curl_setopt($ch,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); 

        if (self::METHOD_POST === $method
            || self::METHOD_GET === $method
        ) {
            if (empty($headers)) {
                $headers[] = 'Content-Type: application/json';
                //$headers[] = 'Cache-Control: no-cache';
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if (self::METHOD_GET === $method && !empty($params)) {
            $url .= '?' . $params;
        }
        if (self::METHOD_POST === $method) {
            //curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);

        $info = curl_getinfo($ch);
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($errno) {
            //throw new InValidRequestException($error, $errno);
            var_dump($errno); exit;
        }
        return new ApiResponse(
            $info['http_code'],
            json_decode($response, true),
            "OK",
            $responseHeaders
        );
    }

    //Выполнение запроса POST

    public function executePostRequest( $url,  $data)
    {
        $response = $this->request(
            self::METHOD_POST,
            $url,
            json_encode($data)
        );
        if ($response->getHttpCode() != 201) {
            $errorMessage = $response->getData();
            if (is_array($errorMessage)) {
                $errorMessage = current($errorMessage);
                if (isset($errorMessage[0])) {
                    $response->setMessage($errorMessage[0]);
                }
            }
        }
        return $response;
    }



    //Выполнение запроса на получение списка GET

    public function executeListRequest( $url,  $params = []) 
    {
        $response = $this->request(
            self::METHOD_GET,
            $url,
            http_build_query($params)
        );

        if ($response->getHttpCode() != 200) {
            $errorMessage = $response->getData();
            if (is_array($errorMessage)){
                $errorMessage = current($errorMessage);
                $response->setMessage($errorMessage);
            }
        }
        return $response;
    }

    //Выполнение запроса на получение одной записи

    public function executeGetRequest( $url, $id,  $params = []) 
    {
        $response = $this->request(
            self::METHOD_GET,
            $url,
            http_build_query($params)
        );
        if(!$response->getData()) {
            $response->setHttpCode(404);
            $response->setMessage("Запись '$id' не найдена.'");
        }

        return $response;
    }


}
