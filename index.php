<html>
<head>
    <meta charset="utf-8">
    <title>Добавление контакта </title>
    <link rel="stylesheet" type="text/css" href="style.css" media="all">
</head>
<body>
    <div>
        <h1>Создание контакта russellorv.amocrm.ru</h1>
        
        <?php if(isset($_GET['error'])){ ?>
            <p class="error">заполните все поля</p>
        <?php } ?>
        <?php if(isset($_GET['false'])){ ?>
            <p class="error">ошибка на стороне сервера, повторите отправку </p>
        <?php } ?>
        
        <?php if(isset($_GET['success'])){ ?>
            <p class="success">Отправлено</p>
        <?php } ?>

        <form action="form.php" method="post">
            <div class="field">
                <label for="name">Имя</label>
                <input id="name" type="text" name="name" />
            </div>
            <div class="field">
                <label for="phone">Телефон *</label>
                <input id="phone" type="tel" name="phone"  required />
            </div>
            <div class="field">
                <label for="email">E-mail *</label>
                <input id="email" type="email" name="email" required />
            </div>

            <div class="field">
                <button type="submit">Создать контакт</button>
            </div>
        </form>
    </div>

</body>
</html>
